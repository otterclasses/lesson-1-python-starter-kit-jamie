# Cyberpunk inventory
print("Cyberpunk Management")
inventory = ["dildo", "shirt", "rags"]
equipped = {
  "head": None, 
  "chest": None,
  "legs": "rags",
  "feet": None,
  "weapon": None
}

def is_equipped(name):
  values = equipped.values()
  if name in values:
    return True
  else:
    return False

def view_inventory():
  print("Inventory")
  print("----------")
  length = len(inventory)
  for i in range(0, length):
    name = inventory[i]
    # in the future, this should say what slot it is equipped, not just E
    on_person = ""
    if is_equipped(name):
      on_person = " (e)"
    print("- " + name + on_person)


def pick_up(new_thing):
  inventory.append(new_thing)

pick_up("beer")
pick_up("chips")
pick_up("shoes")


def drop_item(old_thing):
  inventory.remove(old_thing)

drop_item("chips")

def move_item_by_index(index_1, index_2):
  item_switch = inventory[index_1]
  inventory[index_1] = inventory[index_2]
  inventory[index_2] = item_switch

#move_item_by_index(0, 3)

def move_item_by_name(name_1, name_2):
  index_1 = inventory.index(name_1)
  index_2 = inventory.index(name_2)
  move_item_by_index(index_1,index_2)

move_item_by_name("shirt", "beer")

# this should not just always equip shoes
def equip_item(equip_item_where, item_name):
  equipped[equip_item_where] = item_name

equip_item("hand", "dildo")

print(equipped)

view_inventory()