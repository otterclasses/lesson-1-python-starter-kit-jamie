# strings
num1 = "1" + "1"
# numbers
num2 = 1 + 1
# boolean > >= < <= != ==
isThisTrue = 9 > 6
isThisFalse = 6 >= 9
# lists
names = ["kenny", "anne", num1, "taylolr", "carp;line", "tium", "anne again", "jamie", "donkey"]
# lists have index at 0
print(names[1])
# you can change them
names[1] = "anne jones"
print(names[1])
# add to lists
names.append("bruno")
print(names)
# remove from a list
names.remove("bruno")
print(names)

print(names[1])
print(names[1 + 4])
print(names[1 * 2 - 3 + 5 * 3 - 7])
myIndex = 3
print(names[myIndex])
myIndex += 1
print(names[myIndex])
print(myIndex)

# you can loop with a While
counter = 1
while counter <= 10:
  #print(counter)
  counter += 1

print("hello my name is " + names[0])
print("hello my name is " + names[1])
print("hello my name is " + names[2])

# i only do 7, get over it man
name_start = 0
while name_start <= 7:
  print(names[name_start])
  name_start += 1
  
# i'm better tho, i do any number
name_start = 0
while name_start < len(names):
  print(names[name_start])
  name_start += 1

# ADVANCED
for name in names:
  print(name)

players1 = ["taylor", "edward", "anne", "michael"]
players2 = ["jonathan", "kenny", "john", "katie"]

for favorite in players1:
  if favorite == "anne":
    print("happy")
  else:
    print("sad")
