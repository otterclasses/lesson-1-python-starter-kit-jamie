board = [
  [" ", " ", " "],
  [" ", " ", " "],
  [" ", " ", " "]
]

def reuseBoard():
  print(
    board[0][0] + " | " + board[0][1] + " | " + board[0][2]
  )
  print(
    "----------"
  )
  print(
    board[1][0] + " | " + board[1][1] + " | " + board[1][2]
  )
  print(
    "----------"
  )
  print(
    board[2][0] + " | " + board[2][1] + " | " + board[2][2]
  )

reuseBoard()

numTurns = 0

while numTurns < 9:
  isEven = numTurns % 2 == 0

  turn1 = input("where do you want to go X(e.g 1,1 or 2,1)")

  # takes "1,1" and turns it into an array ["1", "1"]
  coords = turn1.split(",")

  x = int(coords[0]) - 1
  y = int(coords[1]) - 1

  if isEven:
    board[y][x] = "X"
  else:
    board[y][x] = "O"

  reuseBoard()
  numTurns = numTurns + 1