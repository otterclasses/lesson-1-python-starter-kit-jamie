import pgzrun
import pygame

WIDTH = 800
HEIGHT = 1000

pacman = {
  "x": 0,
  "y": 0,
  "width": 48,
  "height": 48,
  "speed": 4
  #  "direction": "right",
}
# set start location
pacman["x"] = 400 - pacman['width'] / 2
pacman["y"] = 500


# making a scaled pacman 16x16
image = pygame.image.load("images/pacman.png")
large_pacman = pygame.transform.scale(image, (pacman['width'], pacman['height']))


def pacman_move():
  if keyboard[keys.LEFT]:
    pacman['x'] = pacman['x'] - pacman['speed']
  if keyboard[keys.RIGHT]:
    pacman['x'] = pacman['x'] + pacman['speed']
  if keyboard[keys.UP]:
    pacman['y'] = pacman['y'] - pacman['speed']
  if keyboard[keys.DOWN]:
    pacman['y'] = pacman['y'] + pacman['speed']

  if pacman['x'] > 800 - pacman['width']:
    pacman['x'] = 800 - pacman['width']
  elif pacman['x'] < 0:
    pacman['x'] = 0 

  if pacman['y'] > 1000 - pacman['height']:
    pacman['y'] = 1000 - pacman['height']
  elif pacman['y'] < 0:
    pacman['y'] = 0 


def update():
  pacman_move()

def draw():
  # removes everything drawn last loop
  screen.clear()
  # draw our background
  screen.blit(large_pacman, (pacman['x'], pacman['y']))

pgzrun.go()